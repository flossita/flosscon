#!/usr/bin/env bash

set -x
go run $1/*.go
