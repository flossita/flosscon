package main

import "fmt"

type Contact struct {
	Name string
	Tel string
}

const Juliette = "Juliette"

func main() {
	var juliette Contact

	juliette.Name = Juliette
	juliette.Tel = "+3365374739543"

	fmt.Print(juliette)

	romeo := Contact{
		Name: "Roméo",
		Tel: "+33652235427",
	}

	fmt.Printf("\n\nRoméo et Juliette - Moderne:\n\n%s:\n" +
		" Appelle moi au N°%s avant que j'escalade... \n\nTon %s.\n",
		romeo.Name, romeo.Tel, romeo.Name)

}
