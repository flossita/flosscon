package main

import "fmt"

const (
	Juliette = "Juliette"
	Romeo = "Roméo"
)


type Contacts map[string]*Contact

func main() {
	var Personnages Contacts = make(map[string]*Contact)

	var unContact *Contact
	Personnages[Juliette] = unContact.DefinirPersonnages(Juliette, "+33653747395")
	Personnages[Romeo]    = unContact.DefinirPersonnages(Romeo,    "+33652235427")

	fmt.Print("\n\nRoméo et Juliette - Moderne:\n\n")

	romeoMsg := "Appelle moi au N°%s avant que j'escalade... \n\nTon %s."
	Personnages[Romeo].Message(romeoMsg, Personnages[Romeo].Tel, Personnages[Romeo].Name)

	julietteMsg := "Ca sonne occupé!!! tu téléphones à qui??? \n\nTa %s."
	Personnages[Juliette].Message(julietteMsg, Personnages[Juliette].Name)

}
