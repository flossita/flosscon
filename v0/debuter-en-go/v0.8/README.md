# Introduction

Cet exemple est une refactorisation, essentiellement.
Mais elle introduit la possibilité de quitter le programme avec
un code de retour.

Pour le tester:

Execution immédiate (compile, link, run et supprimer)
`go run main.go`

ou

`go build && ./v0`

# ce qui est abordé

- Code de retour vers le shell
- Recherche de données dans un hash avec test
- L'allocation de mémoire avec make


C larsonneur
