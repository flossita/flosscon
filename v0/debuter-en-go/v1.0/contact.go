package main

import "fmt"

type Contact struct {
	Name string
	Tel  string
}

func (c *Contact)DefinirPersonnages(name, tel string) *Contact {
	if c == nil {
		return &Contact{
			Name: name,
			Tel: tel,
		}

	}
	c.Name = name
	c.Tel = tel
	return c
}

func (c *Contact)Message(message string, params ...interface{}) {
	fmt.Printf("%s:\n%s\n----\n", c.Name, fmt.Sprintf(message, params...))
}
