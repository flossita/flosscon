package main

import (
	"fmt"
	"os"
	"io/ioutil"
	"strings"
)

type Personnages struct {
	Personnes map[string]*Contact
}

func (p *Personnages) Person(name string) (*Contact){
	if v, found := p.Personnes[name] ; !found {
		fmt.Printf("Unable to find person '%s'\n", name)
		os.Exit(1)
		return nil
	} else {
		return v
	}
}

func (p *Personnages) Init(filename string) {
	p.Personnes = make(map[string]*Contact)

	var lines []string
	if data, err := ioutil.ReadFile(filename) ; err != nil {
		fmt.Print(err)
	} else {
		lines = strings.Split(string(data), "\n")
	}
	for _, line := range lines {
		var unContact *Contact
		if line == "" {
			continue
		}
		fields := strings.Split(line, ",")
		p.Personnes[fields[0]] = unContact.DefinirPersonnages(fields[0], fields[1])
	}

}
