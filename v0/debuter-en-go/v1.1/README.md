# Introduction

Cet exemple va lire un fichier ligne par ligne et fera un usage de la
mémoire limité, contrairement à la version 1.0

On introduit également un appel à defer(), petite astuce de GO, très
pratique.

Pour le tester:

Execution immédiate (compile, link, run et supprimer)
`go run main.go`

ou

`go build && ./v0`

# ce qui est abordé

- Quelques modules de traitements de fichiers et de buffer
- Plusieurs paramètres de retour des fonctions



C larsonneur
