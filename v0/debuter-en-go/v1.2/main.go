package main

import (
	"fmt"
)

const (
	Juliette = "Juliette"
	Romeo = "Roméo"
)


type Contacts map[string]*Contact


func main() {
	var personnages Personnages

	personnages.Init("v1.2/personnes.yml")

	romeo := personnages.Person(Romeo)
	juliette := personnages.Person(Juliette)

	fmt.Print("\n\nRoméo et Juliette - Moderne:\n----\n")

	romeo.   Message("Appelle moi au N°%s avant que j'escalade... \n\nTon %s.", romeo.Tel, romeo.Name)
	juliette.Message("Ca sonne occupé!!! tu téléphones à qui??? \n\nTa %s.", juliette.Name)
	romeo.   Message("C'est ma mère qui me demande ou je suis! \n\nTon %s.", romeo.Name)
	juliette.Message("ha oui? Et tu lui dis cherie à ta mère??? je t'ai entendu de mon balcon ducon!")
}

